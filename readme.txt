To run:
java REcompile "<regExp>" | java REsearch <text file>
EG)
java REcompile "a*c" | java REsearch test.txt

Normal brackets "()", does not work in the FSM compile output neither does "\".

Compile works with somewhat complex input but mostly only tested fairly simple inputs. Same with the search.
