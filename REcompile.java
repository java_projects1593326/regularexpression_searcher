//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.util.ArrayList;

public class REcompile {
    //Our context free grammar
    // E -> T
    // E -> T E
    // T -> F
    // T -> F*
    // T -> F+
    // T -> F?
    // T -> F | T
    // F -> in vocab
    // F -> .
    // F -> (E)
    // F -> [E]
    // F -> SPECIAL CHARACTER \

    //Initailizes variables.
    public static char[] regExp;
    public static ArrayList<Character> regExp2 = new ArrayList<Character>();
    public static int j = 0;
    static int state = 1; 
    static ArrayList<Character> ch = new ArrayList<Character>();
    static ArrayList<Integer> next1 = new ArrayList<Integer>();
    static ArrayList<Integer> next2 = new ArrayList<Integer>();
    static boolean error;

    
    public static void main(String[] args){
        try{
            //Turns first argument to regular expression char array.    
            regExp = args[0].toCharArray();

            //Inputs char array into regular expression array list.
            for(int i =0; i < regExp.length; i++){
                System.out.print(regExp[i]);
                regExp2.add(regExp[i]);
                
            }
            System.out.println(" ");
            set_state(0, '-', 1, 1);
            parse();
            
        }catch(Exception e){
            if(!error){
                //Prints the FSM results.
                printFSM();
            }
        }
    }

    //Expression method.
    public static int expression(){
        int r = 0;
        //Call term and make r equal to result.
        r = term();
        //If its valid in our context free grammer than call expression.
        if(isVocab(regExp2.get(j)) || regExp2.get(j) == '.' || regExp2.get(j) == '\\' || regExp2.get(j) == '(' || regExp2.get(j) == '['){
            expression();
        }
        return r;
    }

    //Call term.
    public static int term(){
        int r;
        int t1;
        int t2;
        int f;
        r = t1 = factor();
        f = state - 1;
        
        if(regExp2.get(j) == '*'){// if a * is there zero or more
            if(next1.get(f) == next2.get(f)){
                set_state(state -1 , '-', state + 1, t1 +1);
                next1.set(state, state +1);
                next2.set(state, state);
            }
            j++;
            r = state;
            state ++;
        }
        else if(regExp2.get(j) == '+'){// if a + is there one or more
            set_state(state, '-', t1, state + 1);
            j++;
            r = state;
            state ++;
        }
        else if(regExp2.get(j) == '?'){// if a ? is there zero or one
            if(next1.get(f) == next2.get(f)){
                set_state(state -1 , '-', state + 1, t1 +1);
                next1.set(state, state +2);
                next2.set(state, state +2);
                state++;
                set_state(state, '-', state +1, state +1);
            }

            r=state;
            j++;
            state++;
        }
        else if(regExp2.get(j)=='|'){ // If there is a | use alternation.
            set_state(state -1, '-', state, state + 1);
            next1.set(state, state +2);
            next2.set(state, state +2);
            state++;
            
            j++;
        }
        return r;
    }

    public static int factor(){
        int r = 0;
        if(isVocab(regExp2.get(j))){ // If j is part of the vocab.
            set_state(state, regExp2.get(j), state  +1, state +1);
            j++;
            r = state;
            state ++;
        }
        else if(regExp2.get(j) == '.'){//if there is a . wildcard
            set_state(state, regExp2.get(j), state  +1, state +1);
            j++;
            r = state;
            state ++;
        }
        else if(regExp2.get(j) == '\\'){//if there is a \
            if(!isVocab(regExp2.get(j)) && isSpecialChar(regExp2.get(j-1))){
                j++;
            }else{
                error();
            }
        }
        else if(regExp2.get(j) =='('){//if there is a (
            j++;
            if(j == regExp2.size()){
                error();    
            }
            expression();
            if(regExp2.get(j) == ')'){
                j++;
            }
            else{
                error();
            }
        }
        else if(regExp2.get(j) == '['){//if there is a [
            int index = regExp2.indexOf('[');
            int index2 = regExp2.indexOf(']');
            j++;
            
            if(j == regExp2.size()){
                error();
            }
            //Calls shorthand alternation.
            regExp2 = shortHandAlternation(index, index2);

            expression();
            if(regExp2.get(j) == ']'){
                
                j++;
            }else{
                error();
            }
        }
        else{
            error();
        }
        return r;
    }

    
    //Method that changes regular expression. All character in square brackets
    //have alternation symbol placed between them. EG) [abc] == a|b|c.
    public static ArrayList<Character> shortHandAlternation(int index1, int index2){
        ArrayList<Character> regExp3 = new ArrayList<Character>();
        regExp3.addAll(regExp2);
        int counter = 1;
        int num = index1 + 1;
        //index2= index2 - 1;

        for(int i =0; i <=index2; i++){
            if(Character.isLetterOrDigit(regExp2.get(num)) && regExp2.get(num + 1) != ']'){
                regExp3.add(num+counter, '|');
                num++;
                counter++;
            }
        }
        return regExp3;
    }

    //Starts parsing of regExp.
    public static void parse(){
        int initial = 0;
        initial = expression();
        
        if(regExp2.get(j) != '\0'){
            //set error
            error();
        }
    }

    //Shows error is invalid regular expression.
    public static void error(){
        System.err.println("There was an error in your regular expression");
        error = true;   
        return;
    }

    //Checks if input is part of the vocabulary.
    public static boolean isVocab(char c){
        if(Character.isLetterOrDigit(c)){
            return true;
        }
        return false;
    }

    //Checks if special character.
    public static boolean isSpecialChar(char c){
        if(c == '*' || c == '+' || c == '+' || c == '?' || c == '|'){
            return true;
        }else{
            return false;
        }
    }

    //Prints the fsm in a table format.
    public static void printFSM(){
        set_state(state, '-', 0, 0);
        System.out.println("s   ch 1 2 ");
        System.out.println("--+--+-+-+-");
        for(int i = 0; i < ch.size(); i++){
            System.out.print(i + " | ");
            System.out.print(ch.get(i) + " ");
            System.out.print(next1.get(i) + " ");
            System.out.print(next2.get(i) + " ");
            System.out.println(" ");
        }
    }

    //Sets the state in the FSM.
    public static void set_state(int s, char c, int n1, int n2){
        ch.add(s, c);
        next1.add(s, n1);
        next2.add(s, n2);
    }
}

