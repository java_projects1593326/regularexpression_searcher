//Adam Williams 1518245.
//Joseph Smith 1578087.

import java.io.*;
import java.util.*;

public class REsearch {
    //Initailises variables and lists.
    public static ArrayList<String> charList = new ArrayList<String>();
    public static ArrayList<Integer> firstState = new ArrayList<Integer>();
    public static ArrayList<Integer> secondState = new ArrayList<Integer>();
    public static Deque<Integer> currentStates = new ArrayDeque<Integer>();

    public static boolean resetFsm = false;
    public static boolean match;

    public static void main(String[] args){

        try{
            //Initilise standard input reader.
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

            String line = input.readLine();
            line = input.readLine();
            line = input.readLine();
            line = input.readLine();

            //Reads standarad input and saves the state by adding to arraylists.
            while(line != null){
                
                String[] values = line.split(" ");
                charList.add(values[2]);
                firstState.add(Integer.valueOf(values[3]));
                secondState.add(Integer.valueOf(values[4]));
                line = input.readLine();

            }
            //Removes last state input and puts and end state at the end instead.
            charList.remove(charList.size() - 1);
            firstState.remove(firstState.size() - 1);
            secondState.remove(secondState.size() - 1);

            charList.add("END");
            firstState.add(-1);
            secondState.add(-1);
            //Outputs the FSM.
            for(int i = 0; i < charList.size(); i++){
                System.out.print(charList.get(i));
                System.out.print(firstState.get(i));
                System.out.println(secondState.get(i));
            }

            input.close();
            //Initilises file reader.
            BufferedReader file = new BufferedReader(new FileReader(args[0]));

            line = file.readLine();
            int lineNum = 0;

            //While a line still exists.
            while(line !=null){
                int currentChar = 0;
                currentStates.addFirst(0);

                //While the current char is less than the total length of the line.
                    while(currentChar < line.length()){
                        //Resets the FSM if search unsuccessful.
                        if(resetFsm == true){
                            currentStates.clear();
                            currentStates.add(0);
                            resetFsm = false;
                        }
                        int s = 0;

                        //Gets the current state.
                        s = currentStates.removeFirst();
                        //If its an end state print where the patern match was found.
                        if(charList.get(s).contains("END")){
                            match = true;
                            System.out.println("Index " + currentChar + ", Line Num: " + lineNum + ", Regular expression found in this line: " + line);
                            break;
                        }
                        //If branching state push next state onto current states.
                        if(charList.get(s).contains("-")){
                            pushStates(s);
                        }
                        //Seach if the state character matches the current character in the line.
                        else{
                            char c = charList.get(s).charAt(0);
                            String matchingChar = String.valueOf(c);
                            String lineChar = String.valueOf(line.charAt(currentChar));
                            //If character matches push nexts state onto deque.
                            if(matchingChar.contains(lineChar)){
                                    pushStates(s);
                                    currentChar ++;
                            }
                            //If deque is empty than reset the FSM.
                            else if(currentStates.size() == 0){
                                resetFsm = true;
                                currentChar++;
                            }
                        }
                }
                //Go to next line.
                line = file.readLine();
                lineNum ++;
            }
            file.close();
        }
        catch(Exception e){
            System.out.println(e);


        }
    }

    //Pushes states on to the deque.
    public static void pushStates(int currentState){
        int first = firstState.get(currentState);
        int second = secondState.get(currentState);

        //If they are equal add one state.
        if(first == second){
            currentStates.addLast(first);
        }
        //Orders state from low to high in deque.
        else{
            if(first < second){
                currentStates.addLast(first);
                currentStates.addLast(second);
            }
            else{
                currentStates.addLast(second);
                currentStates.addLast(first);
            }
        }
    }
    //Clears the deque.
    public static void removeStates(){
        currentStates.clear();
    }
}
